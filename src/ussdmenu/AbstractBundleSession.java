/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ussdmenu;

/**
 *
 * @author vagrant
 */
public abstract class AbstractBundleSession {
    private String ussdSessionId;
    private String msisdn;
    private AbstractUserOption selectedOption;

    /**
     * @return the ussdSessionId
     */
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    /**
     * @param ussdSessionId the ussdSessionId to set
     */
    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the selectedOption
     */
    public AbstractUserOption getSelectedOption() {
        return selectedOption;
    }

    /**
     * @param selectedOption the selectedOption to set
     */
    public void setSelectedOption(AbstractUserOption selectedOption) {
        this.selectedOption = selectedOption;
    }
    
}
