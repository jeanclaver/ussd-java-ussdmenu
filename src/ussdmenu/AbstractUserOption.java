/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ussdmenu;

/**
 *
 * @author vagrant
 */
public class AbstractUserOption {
   private int price;
   private String optionText;
   private String confirmText;
   private String refillId; 

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the optionText
     */
    public String getOptionText() {
        return optionText;
    }

    /**
     * @param optionText the optionText to set
     */
    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    /**
     * @return the confirmText
     */
    public String getConfirmText() {
        return confirmText;
    }

    /**
     * @param confirmText the confirmText to set
     */
    public void setConfirmText(String confirmText) {
        this.confirmText = confirmText;
    }

    /**
     * @return the refillId
     */
    public String getRefillId() {
        return refillId;
    }

    /**
     * @param refillId the refillId to set
     */
    public void setRefillId(String refillId) {
        this.refillId = refillId;
    }
   
}
