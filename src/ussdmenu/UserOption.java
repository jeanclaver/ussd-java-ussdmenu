/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ussdmenu;

/**
 *
 * @author vagrant
 */
public class UserOption extends AbstractUserOption {
    private String optionText;
    private String confirmText;
    private String keyword;
    private int mainDataValue;
    private int bonusDataValue;
    private int totalDataValue;

    /**
     * @return the optionText
     */
    public String getOptionText() {
        return optionText;
    }

    /**
     * @param optionText the optionText to set
     */
    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    /**
     * @return the confirmText
     */
    public String getConfirmText() {
        return confirmText;
    }

    /**
     * @param confirmText the confirmText to set
     */
    public void setConfirmText(String confirmText) {
        this.confirmText = confirmText;
    }

    /**
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * @param keyword the keyword to set
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * @return the mainDataValue
     */
    public int getMainDataValue() {
        return mainDataValue;
    }

    /**
     * @param mainDataValue the mainDataValue to set
     */
    public void setMainDataValue(int mainDataValue) {
        this.mainDataValue = mainDataValue;
    }

    /**
     * @return the bonusDataValue
     */
    public int getBonusDataValue() {
        return bonusDataValue;
    }

    /**
     * @param bonusDataValue the bonusDataValue to set
     */
    public void setBonusDataValue(int bonusDataValue) {
        this.bonusDataValue = bonusDataValue;
    }

    /**
     * @return the totalDataValue
     */
    public int getTotalDataValue() {
        return totalDataValue;
    }

    /**
     * @param totalDataValue the totalDataValue to set
     */
    public void setTotalDataValue(int totalDataValue) {
        this.totalDataValue = totalDataValue;
    }

    
}
