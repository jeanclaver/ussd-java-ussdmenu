/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ussdmenu;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author vagrant
 */
public class MainMenu extends AbstractMainMenu  {
    
    
    public MainMenu(String msisdnStr)  {
        super(msisdnStr);
    }
    
    protected void Init(String msisdnStr) {        
          
                                               
        userOptions = new LinkedHashMap();
        

        UserOption userOption = new UserOption();
        userOption.setPrice(1000);
        userOption.setKeyword("30MB_PREPAID");
        //userOption.setOptionEnum(OptionEnum.THIRTY_MB);
        userOption.setMainDataValue(30);
        userOption.setBonusDataValue(10);
	userOption.setOptionText(userOption.getMainDataValue()+"MB+"+userOption.getBonusDataValue()+"MB ("+userOption.getPrice()+" F)");
        userOption.setConfirmText("Vous serez debite de "+userOption.getPrice()+" GNF pour beneficier de "+userOption.getTotalDataValue()+" Mb de connection internet valable 1 jour");
        userOptions.put("1", userOption);
        
        userOption = new UserOption();
        userOption.setPrice(2000);
        userOption.setKeyword("100MB_PREPAID");
        //userOption.setOptionEnum(OptionEnum.TWENTY_MB);
	userOption.setMainDataValue(100);
        userOption.setBonusDataValue(20);
	userOption.setOptionText(userOption.getMainDataValue()+"+"+userOption.getBonusDataValue()+"("+userOption.getPrice()+")");
        userOption.setConfirmText("Vous serez debite de "+userOption.getPrice()+" GNF pour beneficier de "+userOption.getTotalDataValue()+" Mb de connection internet valable 1 jour");
        userOptions.put("2", userOption);

        userOption = new UserOption();
        userOption.setPrice(3000);
        userOption.setKeyword("125MB_PREPAID");
        //userOption.setOptionEnum(OptionEnum.SEVENTYFIVE_MB);
	userOption.setMainDataValue(125);
        userOption.setBonusDataValue(75);
	userOption.setOptionText(userOption.getMainDataValue()+"+"+userOption.getBonusDataValue()+"("+userOption.getPrice()+")");
        userOption.setConfirmText("Vous serez debite de "+userOption.getPrice()+" GNF pour beneficier de "+userOption.getTotalDataValue()+" Mb de connection internet valable 1 jour");
        userOptions.put("3", userOption);
        
        userOption = new UserOption();
        userOption.setPrice(6500);
        userOption.setKeyword("250MB_PREPAID");        
        //userOption.setOptionEnum(OptionEnum.TWO_HUNDRED_AND_FIFTY_MB);
	userOption.setMainDataValue(250);
        userOption.setBonusDataValue(250);
	userOption.setOptionText(userOption.getMainDataValue()+"+"+userOption.getBonusDataValue()+"("+userOption.getPrice()+")");
        userOption.setConfirmText("Vous serez debite de "+userOption.getPrice()+" GNF pour beneficier de "+userOption.getTotalDataValue()+" Mb de connection internet valable 1 jour");
        userOptions.put("4", userOption);       

        Process();              
               
    }
    

    
    public String getString(){
        return msg;
    }
        
  
    public void Process(){
        StringBuilder sb = new StringBuilder();
        sb.append("Forfaits Jour\n");
	
        
        
               
        // Get a set of the entries
        Set set = userOptions.entrySet();
        // Get an iterator
        Iterator i = set.iterator();
        // Display elements
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            String key = (String) me.getKey();
            UserOption userOption = (UserOption) me.getValue();
            sb.append(key+" - "+userOption.getOptionText()+"\n");
        }
              
        //sb.append("\n");
        sb.append("Repondez");
        msg = sb.toString();
        
    }
    
    
    
    

    
}

