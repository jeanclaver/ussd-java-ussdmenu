/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ussdmenu;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 *
 * @author vagrant
 */
public abstract class AbstractMainMenu implements Serializable  {
    
    protected LinkedHashMap userOptions = new LinkedHashMap();
    protected String msg;
    
    public AbstractMainMenu(String msisdnStr)  {    
        Init(msisdnStr);
    }
    
    protected abstract void Init(String msisdnStr);
        
    public String getString(){
        if (msg ==null) 
            throw new RuntimeException("Main Menu class not properly initialised");
        else
            return msg;
    }
        
 
    
    public AbstractUserOption getOption(String input){
        AbstractUserOption  segment = null;
        Object obj = userOptions.get(input);
        if(obj != null){
            segment = (AbstractUserOption ) obj;
        }
        
        return segment;       
    }    
}